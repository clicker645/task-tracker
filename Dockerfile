FROM node:14.2.0-alpine

RUN mkdir -p /var/www/task-tracker && chown -R node:node /var/www/task-tracker
WORKDIR /var/www/task-tracker

USER node

COPY --chown=node:node . /var/www/task-tracker

RUN npm install && npm run build

ENV NODE_ENV production

CMD ["npm", "run", "start:prod"]
