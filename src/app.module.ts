import { MiddlewareConsumer, Module, NestModule } from '@nestjs/common';
import { GraphQLModule } from '@nestjs/graphql';

import { DocumentHistoryModule } from './app/document-history/document-history.module';
import { AuthModule } from './app/auth/auth.module';
import { UserModule } from './app/user/user.module';
import { ItemModule } from './app/todo-item/item.module';
import { ShareModule } from './app/share/share.module';
import { appConstants } from './config/config.const';

@Module({
  imports: [
    GraphQLModule.forRoot({
      debug: false,
      autoSchemaFile: appConstants.defaultGraphQLSchemaName,
      context: ({ req }) => ({ req }),
    }),
    DocumentHistoryModule,
    AuthModule,
    ShareModule,
    UserModule,
    ItemModule,
    //CasbinModule,
  ],
  controllers: [],
})
export class AppModule {
  //implements NestModule {
  //  configure(consumer: MiddlewareConsumer): any {
  //    consumer.apply(CasbinRBACMiddleware).forRoutes(UserController);
  //  }
}
