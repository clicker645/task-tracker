import {
  Injectable,
  BadRequestException,
  UnauthorizedException,
} from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { ConfigService } from '@nestjs/config';
import bcrypt from 'bcrypt';

import { LoginDto } from './dto/login.dto';
import { ITokenStorage } from './interfaces/token.storage';
import { UserService } from '../user/user.service';
import { TokenDto } from './dto/token.dto';
import { AuthEntity } from './auth.entity';
import { dictionary } from '../../config/dictionary';
import { JwtFromRequestFunction } from 'passport-jwt';
import { Request } from 'express';
import { User } from '../user/user.entity';
import { authConstants } from '../../config/auth.config';

@Injectable()
export class AuthService {
  constructor(
    private readonly jwtService: JwtService,
    private readonly tokenStorage: ITokenStorage,
    private readonly configService: ConfigService,
    private readonly userService: UserService,
  ) {}

  async userLogin(user: User) {
    const jwtToken = this.jwtService.sign({
      id: user.id,
      role: user.role,
      email: user.email,
    } as TokenDto);

    const expiresIn = authConstants.jwt.tokenLifetime;

    const response = new AuthEntity(expiresIn, jwtToken, user);
    await this.tokenStorage.set(user.id, response, expiresIn);

    return response;
  }

  async login(payload: LoginDto): Promise<AuthEntity> {
    const user = await this.userService.findByEmail(payload.email);
    const isCompare = await bcrypt.compare(payload.password, user.password);
    if (!isCompare) {
      throw new BadRequestException(dictionary.errors.passwordMatchError);
    }

    return this.userLogin(user);
  }

  async verify(
    extractor: JwtFromRequestFunction,
    req: Request,
  ): Promise<TokenDto> {
    const token = extractor(req);
    if (!token) {
      throw new UnauthorizedException();
    }

    return this.jwtService.verifyAsync(token);
  }

  async loginByEmail(req) {
    if (!req.user) {
      return 'No user from google';
    }

    const user = await this.userService.findByEmailOrCreate(req.user.email);
    return this.userLogin(user);
  }
}
