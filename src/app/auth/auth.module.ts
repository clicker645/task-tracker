import { Module } from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt';
import { PassportModule } from '@nestjs/passport';
import { ConfigService } from '@nestjs/config';

import { authProviders } from './auth.provider';
import { AuthController } from './auth.controller';
import { UserModule } from '../user/user.module';
import { AuthResolver } from './auth.resolver';
import { RedisModule } from '../../infrastructure/databases/redis/redis.module';
import { RedisService } from '../../infrastructure/databases/redis/redis.service';
import { AuthService } from './auth.service';
import { authConstants, authStrategy } from '../../config/auth.config';

const jwtPassportModule = PassportModule.register({
  defaultStrategy: authStrategy.jwt,
});

@Module({
  imports: [
    UserModule,
    RedisModule.forRoot(),
    jwtPassportModule,
    JwtModule.register({
      secret: authConstants.jwt.secret,
      signOptions: {
        expiresIn: +authConstants.jwt.tokenLifetime,
      },
    }),
  ],
  controllers: [AuthController],
  providers: [...authProviders, RedisService, AuthResolver],
  exports: [jwtPassportModule, AuthService],
})
export class AuthModule {}
