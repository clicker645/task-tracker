import { PassportStrategy } from '@nestjs/passport';
import { Strategy, VerifyCallback } from 'passport-google-oauth20';

import { Injectable } from '@nestjs/common';
import { authConstants, authStrategy } from '../../../config/auth.config';

@Injectable()
export class GoogleStrategy extends PassportStrategy(
  Strategy,
  authStrategy.google,
) {
  constructor() {
    super({
      clientID: authConstants.google.clientID,
      clientSecret: authConstants.google.clientSecret,
      callbackURL: authConstants.google.callbackURL,
      scope: authConstants.google.scope,
    });
  }

  async validate(
    accessToken: string,
    refreshToken: string,
    profile: any,
    done: VerifyCallback,
  ): Promise<any> {
    // TODO need to set googleID in response

    const { name, emails, photos } = profile;
    const user = {
      email: emails[0].value,
      firstName: name.givenName,
      lastName: name.familyName,
      picture: photos[0].value,
      accessToken,
    };
    done(null, user);
  }
}
