import { ExtractJwt, Strategy } from 'passport-jwt';
import { PassportStrategy } from '@nestjs/passport';
import { Injectable, UnauthorizedException } from '@nestjs/common';

import { ITokenStorage } from '../interfaces/token.storage';
import { AuthEntity } from '../auth.entity';
import { authConstants } from '../../../config/auth.config';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
  constructor(private readonly tokenStorage: ITokenStorage) {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      secretOrKey: authConstants.jwt.secret,
    });
  }

  async validate({ id }) {
    const loginResponse = (await this.tokenStorage.get(id)) as AuthEntity;
    if (!loginResponse) {
      throw new UnauthorizedException();
    }

    delete loginResponse.user.password;
    return loginResponse.user;
  }
}
