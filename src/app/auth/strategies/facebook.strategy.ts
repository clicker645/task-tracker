import { PassportStrategy } from '@nestjs/passport';
import { VerifyCallback } from 'passport-google-oauth20';
import { Strategy } from 'passport-facebook';
import { authConstants, authStrategy } from '../../../config/auth.config';

export class FacebookStrategy extends PassportStrategy(
  Strategy,
  authStrategy.facebook,
) {
  constructor() {
    super({
      clientID: authConstants.facebook.clientID,
      clientSecret: authConstants.facebook.clientSecret,
      callbackURL: authConstants.facebook.callbackURL,
    });
  }

  async validate(
    accessToken: string,
    refreshToken: string,
    profile: any,
    done: VerifyCallback,
  ): Promise<any> {
    done(null, profile);
  }
}
