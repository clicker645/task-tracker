export enum authStrategy {
  facebook = 'facebook',
  google = 'google',
  jwt = 'jwt',
}

export const authConstants = {
  google: {
    clientID:
      '24602438696-drg7rd7sbmi842nri86637g164g0uha4.apps.googleusercontent.com',
    clientSecret: 'IDpKu2GaAkC7G7tPDvrhD2BS',
    callbackURL: 'http://02e412abd745.ngrok.io/auth/google/redirect',
    scope: ['email', 'profile', 'openid'],
  },
  facebook: {
    clientID: '343919597002232',
    clientSecret: 'eb776cbd07750e5b434dccd6d350302b',
    callbackURL: 'https://02e412abd745.ngrok.io/auth/facebook/redirect',
  },
  jwt: {
    secret: 'SomeSecretKey',
    tokenLifetime: 86400,
  },
};
